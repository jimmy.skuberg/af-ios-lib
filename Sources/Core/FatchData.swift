//
//  File.swift
//  
//
//  Created by piyawat kunama on 24/5/2565 BE.
//

import Foundation

func getAFData (appKey:String, adKey:String, completion: @escaping ([String: Any]?, Error?) -> Void) {
    
    let json: [String: Any] = ["appKey": appKey,"adKey": adKey]
    let jsonData = try? JSONSerialization.data(withJSONObject: json)
    let url = URL(string: "https://api-afbrother.skuberg.pro/application/render_application_ads")!
    let session = URLSession.shared
    var request = URLRequest(url: url)
    request.httpMethod = "POST"
    request.httpBody = jsonData
    request.addValue("application/json", forHTTPHeaderField: "Content-Type")
    request.addValue("application/json", forHTTPHeaderField: "Accept")
    
    let task = session.dataTask(with: request, completionHandler: { data, response, error in
        
        guard error == nil else {
            completion(nil, error)
            return
        }
        
        guard let data = data else {
            completion(nil, NSError(domain: "dataNilError", code: -100001, userInfo: nil))
            return
        }
        
        do {
            //create json object from data
            guard let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] else {
                completion(nil, NSError(domain: "invalidJSONTypeError", code: -100009, userInfo: nil))
                return
            }
            
            completion(json, nil)
        } catch let error {
            
            completion(nil, error)
        }
    })
    
    task.resume()
}

//
//  File.swift
//  
//
//  Created by piyawat kunama on 25/5/2565 BE.
//

import Foundation
import UIKit

class RenderAdController {
    
    @objc class func renderAd(appKey:String, adKey:String, x: Double ,y: Double, adType: String) -> UIImageView{
        let group = DispatchGroup()
        let ad = UIImageView()
        group.enter()
        
        getAFData(appKey: appKey, adKey: adKey) { (result, error) in
            if let result = result {
                var url = ""
                var width:Double = 0
                var height:Double = 0
                var href = ""
                var key = ""
                for index in result {
                    if(index.key == "url"){
                        url = index.value as! String
                    }
                    if(index.key == "href"){
                        href = index.value as! String
                    }
                    if(index.key == "width"){
                        let dataWidth = index.value
                        width = dataWidth as! Double
                    }
                    if(index.key == "height"){
                        let dataHeight = index.value
                        height = dataHeight as! Double
                    }
                    if(index.key == "key"){
                        let dataKey = index.value
                        key = dataKey as! String
                    }
                }
                
                DispatchQueue.main.async {
                    ad.imageFrom(url: URL(string:url)!)
                    ad.translatesAutoresizingMaskIntoConstraints = true
                    ad.isUserInteractionEnabled = true
                    
                    let screenW = Double(UIScreen.main.bounds.width)
                    let screenH = Double(UIScreen.main.bounds.height)
                    let scale = screenW / width
                    let window = UIApplication.shared.keyWindow
                    let screenSize: CGRect = UIScreen.main.bounds
                    switch adType {
                    case "header":
                        let safeTop = Double(window?.safeAreaInsets.top ?? 0)
                        width = screenW
                        height = scale*height
                        ad.frame = CGRect(x:x,y: safeTop,width: width,height: height)
                    case "sticky":
                        width = screenW
                        height = scale*height
                        let safeButtom = Double(window?.safeAreaInsets.bottom ?? 0)
                        ad.frame = CGRect(x:x,y:screenH - safeButtom - scale*height+y,width: screenW,height: scale*height)
                    case "slide":
                        let safeButtom = Double(window?.safeAreaInsets.bottom ?? 0)
                        ad.frame = CGRect(x:screenW-width+x,y:screenH - safeButtom - height+y,width: width, height: height)
                    case "inPage":
                        ad.frame = CGRect(x:x,y:y,width: width, height: height)
                    default:
                        ad.frame = CGRect(x:x,y:y,width: width, height: height)
                    }
                    
                    let tapAd = CustomTapGestureOnAd(target: self, action: #selector(onClickAd(sender:)))
                    tapAd.href = href
                    ad.addGestureRecognizer(tapAd)
                    
                    
                    let close = UIImageView()
                    close.imageFrom(url: URL(string:"https://api-afbrother.skuberg.pro/api/serverImage/close.png")!)
                    close.translatesAutoresizingMaskIntoConstraints = true
                    close.isUserInteractionEnabled = true
                    close.frame.size = CGSize(width: 20, height: 20)
                    close.backgroundColor = UIColor(white: 1, alpha: 0.3)
                    let tapClose = CustomTapGestureOnClose(target: self, action: #selector(onDeleteAd(sender:)))
                    tapClose.close = close
                    close.addGestureRecognizer(tapClose)
                    
                    ad.addSubview(close)
                    
                    let inScreenWidth = Double(screenSize.intersection(ad.frame).width) >= width-(width*10)/100
                    let inScreenHeight = Double(screenSize.intersection(ad.frame).height) >= height-(width*10)/100
                
                    if(inScreenWidth && inScreenHeight){
                        print("impress")
                        Impression(adServerKey: key, appKey: appKey, adKey: adKey){ (result, error) in
                            if((error) != nil){
                                print(error as Any)
                            }
                        }
                    }
                    
                    group.leave()
                }
            } else if let error = error {
                print("error: \(error.localizedDescription)")
            }
            
        }
        
        return ad
    }
    
    @objc class func onClickAd(sender: CustomTapGestureOnAd) {
        print("sender.href",sender.href ?? "")
        guard let url = URL(string: sender.href ?? "") else { return }
        UIApplication.shared.open(url)
    }
    
    
    @objc class func onDeleteAd(sender: CustomTapGestureOnClose) {
        sender.close?.superview?.removeFromSuperview()
    }
}

class CustomTapGestureOnAd: UITapGestureRecognizer {
    var href: String?
}

class CustomTapGestureOnClose: UITapGestureRecognizer {
    var close: UIImageView?
}

